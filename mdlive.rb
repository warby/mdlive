# mdlive
# Watches the filesystem for changes to .md files, and displays a preview in the 
# user's standard browser on modification.
watch('.*\.md') {|md| 
    
    # Name of file that was modified
    base_filename = File.basename("#{md[0]}", ".md")

    # File name for html output and folder
    output_filename = ".#{base_filename}-mdlive.html"
    output_folder = File.dirname("#{md[0]}")
    output_file = "#{output_folder}/#{output_filename}"

    # Generate html file
    system("markdown_py #{md[0]} > #{output_file}")

    # Styles
    file = File.open("#{ENV['MDLIVE_DIR']}/mdlive.css", "rb")
    css = file.read

    # Document title
    title = File.open("#{md[0]}").first.sub(/[#]*/, "")

    # Head matter
    head =<<-HEND
            <!DOCTYPE html>
            <html>
                <head>
                    <title>#{title}</title>
                    <style>
                        body {
                            margin: 40px auto!important;
                            width: 800px;
                        }

                        body img {
                            display: block;
                            margin: 25px auto;
                        }

                        body h2 {
                            border-bottom: 1px solid #ccc;
                            margin: 20px 0 15px !important;
                            padding-bottom: 5px;
                        }
                        
                        body h3 {
                            border-bottom: 1px solid #eee;
                            margin: 10px 0 10px !important;
                            padding-bottom: 2.5px;
                        }

                        body pre {
                            color: #d14;
                        }

                        body span.comment {
                            color: gray;
                        }

                        body span.comment::before {
                            content: "// "
                        }

                        #{css}
                    </style>
                </head>
                <body>
        HEND

    system("echo '#{head}' | cat - #{output_file} > /tmp/out && mv /tmp/out #{output_file}")
    tail =<<-TEND
                </body>
            </html>
        TEND

    system("echo '#{tail}' | cat - >> #{output_file}")

    # Open html file
    ["sensible-browser #{output_file}", "xdg-open #{output_file}", "open #{output_file}"].each { |command|
        break if system("#{command}")
    }
}
