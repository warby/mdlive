# Mdlive

This tool watches the current directory tree for changes to Markdown files.  When a .md file is changed, a HTML file is generated from the Markdown, and then
opened in the user's default browser.

## Requires

- Ruby
- Watchr gem (`gem install watchr`, https://github.com/mynyml/watchr)

## Installation

1.  Download the source.  You should have:
    - A file called 'mdlive' (bash script)
    - A folder called 'watchr-plugin', containing the files 'mdlive.css' and 'mdlive.rb'

2.  Extract the files from 'watchr-plugin' to your desired directory.  This can be anywhere, but note that the variable MDLIVE_DIR in the script file ('mdlive') must also 
    point to this location.  The default location is ~/.watchr/plugin/mdlive.

3.  Extract the script file to a location in your PATH variable, such as /usr/local/bin

## Usage

Run 'mdlive' to start watching all .md files in and below the current directory.  You can use any editor you like, and the preview will be opened whenever the file is saved.
